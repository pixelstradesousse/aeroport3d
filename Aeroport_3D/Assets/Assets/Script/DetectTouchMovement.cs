﻿using UnityEngine;
using System.Collections;

public class DetectTouchMovement : MonoBehaviour
{
	const float pinchTurnRatio =  Mathf.PI * 1.5f;  // Mathf.PI / 2; ramzi x
	const float minTurnAngle = 0;// a changer 0
	
	const float pinchRatio = 1;
	const float minPinchDistance = 0;// a changer 0
	
	const float panRatio = 1;
	const float minPanDistance = 0;
	
	/// <summary>
	///   Le delta de l'angle entre deux points de contact
	/// </summary>
	static public float turnAngleDelta;
	/// <summary>
	///   L'angle entre deux points de contact
	/// </summary>
	static public float turnAngle;
	
	/// <summary>
	///   Le delta de la distance entre deux points de contact qui ont été éloigne l'une de l'autre
	/// </summary>
	static public float pinchDistanceDelta;
	/// <summary>
	///   La distance entre deux points de contact qui ont été éloigne l'une de l'autre
	/// </summary>
	static public float pinchDistance;
	/// <summary>
	///   Calcule Pincez et tournez - Cela devrait être utilisé à l'intérieur LateUpdate
	/// </summary>
	static public void Calculate () 
	{
		pinchDistance = pinchDistanceDelta = 0;
		turnAngle = turnAngleDelta = 0;
		
		// si deux doigts touchent l'écran en même temps ...
		if (Input.touchCount == 2) 
		{
			Touch touch1 = Input.touches[0];
			Touch touch2 = Input.touches[1];
			
			// ... Si au moins l'un d'eux se déplace ...
			if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved) 
			{
				// ... vérifier la distance delta entre touch1 et touch2 ...
				pinchDistance = Vector2.Distance(touch1.position, touch2.position);
				float prevDistance = Vector2.Distance(touch1.position - touch1.deltaPosition,
				                                      touch2.position - touch2.deltaPosition);
				pinchDistanceDelta = pinchDistance - prevDistance;
				
				// ... si elle est supérieure à un seuil minimal, ce est une pincée!
				if (Mathf.Abs(pinchDistanceDelta) > minPinchDistance)
				{
					pinchDistanceDelta *= pinchRatio;
				} else
				{
					pinchDistance = pinchDistanceDelta = 0;
				}

				// ...ou vérifier l'angle delta entre eux...
				turnAngle = Angle(touch1.position, touch2.position);
				float prevTurn = Angle(touch1.position - touch1.deltaPosition,
				                       touch2.position - touch2.deltaPosition);
				turnAngleDelta = Mathf.DeltaAngle(prevTurn, turnAngle);
				
				// ... si elle est supérieure à un seuil minimal, ce est un tour!
				if (Mathf.Abs(turnAngleDelta) > minTurnAngle) 
				{
					turnAngleDelta *= pinchTurnRatio;
				} else
				{
					turnAngle = turnAngleDelta = 0;
				}
			}
		}
	}
	
	static private float Angle (Vector2 pos1, Vector2 pos2) 
	{
		Vector2 from = pos2 - pos1;
		Vector2 to = new Vector2(1, 0);
		
		float result = Vector2.Angle( from, to );
		Vector3 cross = Vector3.Cross( from, to );
		
		if (cross.z > 0) {
			result = 360f - result;
		}
		
		return result;
	}
}