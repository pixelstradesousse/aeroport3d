﻿using UnityEngine;
using System.Collections;

public class Script_Camera : MonoBehaviour 
{
	//point de rotation de la camera!!
	public Transform PointRotation;
	public float Sensibilite = 0.3f; // sensibilite de rotation 
	/******/
	private Vector3 _position = new Vector3 (0, 0, 0);
	public GameObject Bloc_0;
	public GameObject SousBloc_0;
	public GameObject ElementsBloc_0;
	// ramzi
	public float pinchAmount = 0;
	public float rotationAmount = 0;
	private int ok_in = 0,ok_out=0;
	private float _temps_tap;
	private float _temps_ecouler;
	private Vector3 _touchPos;
	private Vector3 _touchPoss;
	public int _fieldOfView_MIN = 9;
	public int _fieldOfView_MAX = 90;
	//
	void Start()
	{
		_touchPos = transform.position;
	}
	//
	void Fonction_AjustementEcran()
	{
		if(transform.position.x > 30)
		{
			transform.Translate (-transform.position.x * Time.deltaTime * 0.8f  , 0 , 0);
		}
		if(transform.position.x < -30)
		{
			transform.Translate (-transform.position.x * Time.deltaTime * 0.8f , 0 , 0);
		}
		if(transform.position.z > 20)
		{																						
			transform.Translate (0 , -transform.position.z * Time.deltaTime  , -transform.position.z * Time.deltaTime );
		}
		if(transform.position.z < -20)
		{
			transform.Translate (0 , -transform.position.z * Time.deltaTime ,-transform.position.z * Time.deltaTime);
		}
	}
	//Fonction_DoubleTapZoomIn
	void Fonction_DoubleTapZoomIn()
	{
		if(GetComponent<Camera>().fieldOfView > _fieldOfView_MIN)
		{
			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began && Input.GetTouch(0).tapCount == 2) 
			{
				ok_in = 1;
				_temps_tap = Time.time;
				float x = (-10+20*Input.GetTouch(0).position.x / Screen.width);
				float y = (-10+20*Input.GetTouch(0).position.y / Screen.height);
				float _x=0.0f,_y=0.0f,_z=0.0f;
				if( x < 10)
				{
					_x =(x) + transform.position.x;
				}else
				{
					_x =(-x) + transform.position.x;
				}
				if(y < 10) //
				{
					_y =(y) + transform.position.y;
					_z =(y) + transform.position.z;
				}else 
				{
					_y =(-y) + transform.position.y;
					_z =(-y) + transform.position.z;
				}
				_touchPos = new Vector3(0, _y ,_z );
				_touchPoss = new Vector3(_x, 0 , 0);
			}
			if (ok_in == 1 )
			{
				_temps_ecouler = Time.time;
				pinchAmount += Time.deltaTime * 100;
				transform.position = Vector3.Lerp(transform.position , _touchPos , Time.deltaTime * 4);
				transform.position = Vector3.Lerp(transform.position , _touchPoss , Time.deltaTime * 4);
				if(_temps_ecouler - _temps_tap > 0.8f )
					ok_in = 0;
			}
		}
	}
	//Fonction_TapZoomOut
	void Fonction_TapZoomOut()
	{
		if(GetComponent<Camera>().fieldOfView < _fieldOfView_MAX)
		{
			if (Input.touchCount == 2 
		   	 && Input.GetTouch (0).phase == TouchPhase.Began
		   	 && Input.GetTouch (1).phase == TouchPhase.Began
		   	 && Input.GetTouch (0).tapCount == 1
		   	 && Input.GetTouch (1).tapCount == 1) 
			{
				ok_out = 1;
				_temps_tap = Time.time;
			}
			if (ok_out == 1)
			{
				_temps_ecouler = Time.time;
				GetComponent<Camera>().fieldOfView += Time.deltaTime * 40;
				GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, _fieldOfView_MIN, _fieldOfView_MAX);
				if(_temps_ecouler - _temps_tap > 0.7f )
				{
					ok_out = 0;
				}
			}
		}
	}
	// Fonction_DeplacementXY  ** tt modification sig:rotation sur x perturbe la rotatoin de la camera et son deplacement
	void Fonction_DeplacementXY()
	{
		if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Moved) 
		{
			if(GetComponent<Camera>().fieldOfView <= 20)
			{
				Vector2 _positionTouch = Input.GetTouch (0).deltaPosition;//0.09 pour le y car rotation de 60 sur x
				transform.Translate (-_positionTouch.x * 0.05f , -_positionTouch.y * 0.09f , -_positionTouch.y * 0.05f );
				// rotation de la camera au tour de PointRotation
				PointRotation.transform.Translate(-_positionTouch.x * 0.05f,0,-_positionTouch.y * 0.104f);
			}
			else
				if(GetComponent<Camera>().fieldOfView > 20 && GetComponent<Camera>().fieldOfView <= 30)
			{
				Vector2 _positionTouch = Input.GetTouch (0).deltaPosition;//0.25 pour le y car rotation de 60 sur 
				transform.Translate (-_positionTouch.x * 0.15f , -_positionTouch.y * 0.25f , -_positionTouch.y *0.15f );
				//translation du PointRotation pour suivre la camera
				PointRotation.transform.Translate(-_positionTouch.x * 0.15f,0,-_positionTouch.y * 0.287f);
			}else
				if(GetComponent<Camera>().fieldOfView > 30 && GetComponent<Camera>().fieldOfView <= 60)
			{
				Vector2 _positionTouch = Input.GetTouch (0).deltaPosition;//0.85 pour le y car rotation de 60 sur x
				transform.Translate (-_positionTouch.x * 0.5f, -_positionTouch.y * 0.85f, -_positionTouch.y *0.5f );
				//translation du PointRotation pour suivre la camera
				PointRotation.transform.Translate(-_positionTouch.x * 0.5f,0,-_positionTouch.y * 0.98f);
			}else
				if(GetComponent<Camera>().fieldOfView > 60 && GetComponent<Camera>().fieldOfView <= _fieldOfView_MAX)
			{
				Vector2 _positionTouch = Input.GetTouch (0).deltaPosition;//1.3 pour le y car rotation de 60 sur x
				transform.Translate (-_positionTouch.x *0.75f , -_positionTouch.y *1.3f  , -_positionTouch.y *0.75f);
				//translation du PointRotation pour suivre la camera
				PointRotation.transform.Translate(-_positionTouch.x * 0.75f,0,-_positionTouch.y * 1.5f);
			}
		}
	}
	// Fonction_Transparence
	void Fonction_Transparence(GameObject _gameobject,float _valeur)
	{
		Color _color = _gameobject.GetComponent<Renderer>().material.color;
		_color.a += _valeur;
		_gameobject.GetComponent<Renderer>().material.color = _color;
	}
	// Fonction_Transparence Fix
	void Fonction_TransparenceFix(GameObject _gameobject,float _valeur)
	{
		Color _color = _gameobject.GetComponent<Renderer>().material.color;
		_color.a = _valeur;
		_gameobject.GetComponent<Renderer>().material.color = _color;
	}
	// Fonction_ModifierScale
	void Fonction_ModifierScale(GameObject _gameobject)
	{
		_gameobject.transform.localScale = new Vector3(GetComponent<Camera>().fieldOfView*0.08f,
		                                              GetComponent<Camera>().fieldOfView*0.08f,
		                                              0.0001f);
	}
	//
	void Fonction_Test()
	{
		if( Input.touchCount == 1 && Input.GetTouch(0).tapCount == 1 )
		{
			_position = Input.GetTouch(0).position;
			Ray ray = GetComponent<Camera>().ScreenPointToRay(_position);
			RaycastHit hit ;
			if(Physics.Raycast(ray, out hit) )// Detection de la Zone du collider !!!!!!!!!!
			{
				string name = hit.collider.gameObject.name;
				if( name == "element_5" || name == "element_6")
				{
					print("Nom:"+hit.collider.gameObject.name);
				}
			}
		}
	}
	void Update()
	{
		//Fonction_Test ();
	}
	/******** MAIN *********/
	void LateUpdate() 
	{
		pinchAmount = 0;
		Quaternion _rotation = transform.rotation;
		Quaternion desiredRotation = _rotation;
		DetectTouchMovement.Calculate();
		
		if (Mathf.Abs(DetectTouchMovement.pinchDistanceDelta) > 0)
		{ // zoom
			pinchAmount = DetectTouchMovement.pinchDistanceDelta;
		}
		if (Mathf.Abs (DetectTouchMovement.turnAngleDelta) > 0) 
		{ 
			// rotation de la camera au tour de PointRotation
			rotationAmount = DetectTouchMovement.turnAngleDelta;
			Camera.main.transform.RotateAround(PointRotation.transform.position,Vector3.up, rotationAmount*Sensibilite);
			//rotation du PointRotation / a la camera pour avoir une synchronisation lors du déplacement
			PointRotation.transform.Rotate(Vector3.up * rotationAmount * Sensibilite);
		}
		/*********/
		//Fonction_AjustementEcran ();
		/*********/
		//Fonction_DoubleTapZoomIn ();
		/*********/
		//Fonction_TapZoomOut ();
		/*********/
		Fonction_DeplacementXY ();
		/*********/
		if (GetComponent<Camera>().orthographic)
		{
	
		}
		else
		{
			if(GetComponent<Camera>().fieldOfView <= 30)
			{
				Bloc_0.SetActive(false);
				SousBloc_0.SetActive(true);
				ElementsBloc_0.SetActive(true);
				GetComponent<Camera>().fieldOfView -= pinchAmount * 0.2f;
				GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, _fieldOfView_MIN , _fieldOfView_MAX);
			}else
			{
				Bloc_0.SetActive(true);
				SousBloc_0.SetActive(false);
				ElementsBloc_0.SetActive(false);
				if(GetComponent<Camera>().fieldOfView > 30 && GetComponent<Camera>().fieldOfView <= 60)
				{
					GetComponent<Camera>().fieldOfView -= pinchAmount * 0.5f;
					GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, _fieldOfView_MIN , _fieldOfView_MAX);
				}else
					if(GetComponent<Camera>().fieldOfView > 60 && GetComponent<Camera>().fieldOfView <= _fieldOfView_MAX)
				{
					GetComponent<Camera>().fieldOfView -= pinchAmount;
					GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, _fieldOfView_MIN , _fieldOfView_MAX);
				}
			}

		}
	}

	
}
