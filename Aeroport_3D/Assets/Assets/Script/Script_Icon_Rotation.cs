﻿using UnityEngine;
using System.Collections;

public class Script_Icon_Rotation : MonoBehaviour 
{
	//*ROTATION DES ICONS LORS DE LA ROTATION DE LA CAMERA*//

	void Fonction_Rotation(GameObject _gameobject)
	{
		float rotationAmount = Camera.main.GetComponent<Script_Camera>().rotationAmount;
		float Sensibilite = Camera.main.GetComponent<Script_Camera>().Sensibilite;
		_gameobject.transform.Rotate(Vector3.up * rotationAmount * Sensibilite);
	}
	/**/
	void Update () 
	{
		if(Input.touchCount == 2 && (Input.GetTouch(0).phase == TouchPhase.Moved
		  						 || Input.GetTouch(1).phase == TouchPhase.Moved))
		{
			Fonction_Rotation(gameObject);
		}
	}
}
