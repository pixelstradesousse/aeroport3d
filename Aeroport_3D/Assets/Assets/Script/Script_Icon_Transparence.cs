﻿using UnityEngine;
using System.Collections;

public class Script_Icon_Transparence : MonoBehaviour 
{
	/*MODIFIER TRANSPARENCE ICON LORS DE LA ROTATION DE LA CAMERA*/

	public float _FrequeanceTrans = -0.008f; //-0.008f

	// Fonction_Transparence
	void Fonction_Transparence(GameObject _gameobject,float _valeur)
	{
		Color _color = _gameobject.GetComponent<Renderer>().material.color;
		_color.a += _valeur;
		_gameobject.GetComponent<Renderer>().material.color = _color;
	}
	// Fonction_Transparence Fix
	void Fonction_TransparenceFix(GameObject _gameobject,float _valeur)
	{
		Color _color = _gameobject.GetComponent<Renderer>().material.color;
		_color.a = _valeur;
		_gameobject.GetComponent<Renderer>().material.color = _color;
	}
	// Update is called once per frame
	void Update () 
	{
		if (Camera.main.GetComponent<Camera> ().fieldOfView <= 30)
		{
			if(Camera.main.GetComponent<Camera> ().fieldOfView > 9)
			{
				Fonction_Transparence(gameObject,Camera.main.GetComponent<Script_Camera> ().pinchAmount * _FrequeanceTrans);
			}
		}else
		{
			Fonction_TransparenceFix(gameObject,1);
		}
	}
}
