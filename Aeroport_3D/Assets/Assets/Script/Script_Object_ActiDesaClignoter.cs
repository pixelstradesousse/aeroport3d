﻿using UnityEngine;
using System.Collections;

public class Script_Object_ActiDesaClignoter : MonoBehaviour 
{
	/*ACTIVIER DESACTIVER OBJECT + CLIGNOTEMENT*/

	public TextMesh Text;
	private Vector3 _position = new Vector3 (0, 0, 0);
	private float _TempsPass;
	private float _TempsTap=0;
	private float _TempsClin = 0;
	private int ok=1;

	//
	public void Clignoter ()
	{
		Color _color = gameObject.GetComponent<Renderer>().material.color;
		if (_TempsPass - _TempsClin > 0.2f)
		{
			_TempsClin=Time.time;
			if (ok==1)
			{
				_color.r += 0.05f;
				_color.g += 0.05f;
				_color.b += 0.05f;
				if(_color.r>1)
					ok=0;
			}
			if (ok==0)
			{
				_color.r -= 0.05f;
				_color.g -= 0.05f;
				_color.b -= 0.05f;
				if(_color.r<=0.6)
					ok=1;
			}
		}
		gameObject.GetComponent<Renderer>().material.color = _color;	
	}
	// Update is called once per frame
	void Update () 
	{
		_TempsPass = Time.time;
		Clignoter();
		if (Camera.main.GetComponent<Camera>().fieldOfView > 30) 
		{
			gameObject.GetComponent<Renderer>().enabled=true;
			Text.GetComponent<Renderer>().enabled=true;
		}

		if( Input.touchCount == 1 && Input.GetTouch(0).tapCount == 1 )
		{
			if(_TempsPass - _TempsTap > 0.5f)
			{
				_TempsTap=Time.time;
				_position = Input.GetTouch(0).position;
				Ray ray = Camera.main.GetComponent<Camera>().ScreenPointToRay(_position);
				RaycastHit hit ;
				if(Physics.Raycast(ray, out hit) )// Detection de la Zone du collider !!!!!!!!!!
				{
					string name = hit.collider.gameObject.name;
					if( name == gameObject.name && (gameObject.GetComponent<Renderer>().enabled)==true)
					{
						//print("Nom:"+hit.collider.gameObject.name);
						gameObject.GetComponent<Renderer>().enabled=false;
						Text.GetComponent<Renderer>().enabled=false;
						name="";
					}
					if( name == gameObject.name && gameObject.GetComponent<Renderer>().enabled==false)
					{
						gameObject.GetComponent<Renderer>().enabled=true;
						Text.GetComponent<Renderer>().enabled=true;
					}
				}
			}
		}
	}
}

