﻿using UnityEngine;
using System.Collections;

public class Script_Popup_ActiDesa : MonoBehaviour 
{
	/*ACTIVER DESACTIVER POPUP A SUPPRIMER CAR IL N'EST PAS EFFICACE*/

	private Vector3 _position = new Vector3 (0, 0, 0);
	private float _TempsPass;
	private float _TempsTap=0;
	private float _Frequence = 0.4f;// frequence entre 2 tap su le popup
	private GameObject RectanglePopx;
	private GameObject TextPop;
	private GameObject TrianglePop;
	void Start()
	{
		TrianglePop = GameObject.Find ("Popup/TrianglePop");
		RectanglePopx = GameObject.Find ("Popup/RectanglePopx");
		TextPop = GameObject.Find ("Popup/TextPop"); 
		//
		TrianglePop.GetComponent<MeshRenderer>().enabled = false;
		RectanglePopx.GetComponent<Renderer>().enabled = false;
		TextPop.GetComponent<MeshRenderer>().enabled = false;
		// Print pour voir si le script detecte les sous elements de l'objet Popup
		print (RectanglePopx.name);
		print (TextPop.name);
		print (TrianglePop.name);
	}
	void Update () 
	{
		_TempsPass = Time.time;
		if (Camera.main.GetComponent<Camera>().fieldOfView > 30) 
		{
			/*TrianglePop.GetComponent<MeshRenderer>().enabled = false;
			RectanglePopx.GetComponent<Renderer>().enabled = false;
			TextPop.GetComponent<MeshRenderer>().enabled = false;*/
		}
		if( Input.touchCount == 1 && Input.GetTouch(0).tapCount == 1 )
		{
			if(_TempsPass - _TempsTap > _Frequence)
			{
				_TempsTap=Time.time;
				_position = Input.GetTouch(0).position;
				Ray ray = Camera.main.GetComponent<Camera>().ScreenPointToRay(_position);
				RaycastHit hit ;
				if(Physics.Raycast(ray, out hit) )// Detection de la Zone du collider !!!!!!!!!!
				{
					string name = hit.collider.gameObject.name;
					if( name == gameObject.name && RectanglePopx.GetComponent<Renderer>().enabled == true )
					{
						//print("Nom:"+hit.collider.gameObject.name);
						TrianglePop.GetComponent<MeshRenderer>().enabled = false;
						RectanglePopx.GetComponent<Renderer>().enabled = false;
						TextPop.GetComponent<MeshRenderer>().enabled = false;
						name="";
					}
					if( name == gameObject.name && RectanglePopx.GetComponent<Renderer>().enabled == false)
					{
						TrianglePop.GetComponent<MeshRenderer>().enabled = true;
						RectanglePopx.GetComponent<Renderer>().enabled = true;
						TextPop.GetComponent<MeshRenderer>().enabled = true;
					}
				}
			}
		}
	}
}
