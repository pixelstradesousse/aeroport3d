﻿using UnityEngine;
using System.Collections;

public class Script_Popup_ScaTranRot : MonoBehaviour 
{
	/* sSCALE ,TRANSPARENCE , ROTATION : POPUP A SUPPRIMER CAR IL N'EST PAS EFFICACE*/

	Vector3 PositionInitial;
	float _FieldOfView_MAX;
	float _FieldOfView_MIN;

	// Use this for initialization
	//public Camera camera;
	void Start () 
	{
		PositionInitial = this.transform.position;
		_FieldOfView_MAX = Camera.main.GetComponent<Script_Camera>()._fieldOfView_MAX ;
		_FieldOfView_MIN = Camera.main.GetComponent<Script_Camera>()._fieldOfView_MIN ;
	}
	
	void Fonction_ModifierScale(GameObject _gameobject)
	{
		float _FieldOfView = Camera.main.GetComponent<Camera>().fieldOfView;
		_gameobject.transform.localScale = new Vector3(_FieldOfView * 0.055f,1, _FieldOfView * 0.05f);
		//_gameobject.transform.Translate (0, _FieldOfView * 0.1f ,0);
	}
	void Fonction_Rotation(GameObject _gameobject)
	{
		float rotationAmount = Camera.main.GetComponent<Script_Camera>().rotationAmount;
		float Sensibilite = Camera.main.GetComponent<Script_Camera>().Sensibilite;
		_gameobject.transform.Rotate(Vector3.up * rotationAmount * Sensibilite);
	}
	void Fonction_Translate_Y(GameObject _gameobject)
	{
		float pinchAmount = Camera.main.GetComponent<Script_Camera> ().pinchAmount;
		_gameobject.transform.Translate (Vector3.up * -pinchAmount * 0.015f);
	}
	// Update is called once per frame
	void Update () 
	{

		if(Input.touchCount == 2 && (Input.GetTouch(0).phase == TouchPhase.Moved ||
		                            Input.GetTouch(1).phase == TouchPhase.Moved))
		{
			Fonction_Rotation(gameObject);
			if(Camera.main.GetComponent<Camera> ().fieldOfView > 30 )//lorsqu'on sort du Bloc-0
			{
				Fonction_ModifierScale (gameObject);
			}
		}
		if(_FieldOfView_MIN < Camera.main.GetComponent<Camera>().fieldOfView 
		   && Camera.main.GetComponent<Camera>().fieldOfView < _FieldOfView_MAX )
		{
			Fonction_Translate_Y (gameObject);
		}
	}
}
